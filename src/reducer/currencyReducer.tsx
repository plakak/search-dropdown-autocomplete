import { Reducer } from "react";

function currencyReducer(state: currencyState, action: currencyAction) {
    switch (action.type) {
        case currencyActionKind.INITIAL:
            
            return {
                ...state,
                allCurrency: action.payload,
                tempAllCurrency: action.payload
            }
        case currencyActionKind.FILTER:
            return {
                ...state,
                tempAllCurrency: action.payload
            }
        default:
            return {
                ...state,
                searchCurrency: action.payload
            }
    }
}

export {
    currencyReducer
}

enum currencyActionKind {
  INITIAL = "initial",
  SEARCH = "search",
  FILTER = "filter",
}

export interface currencyAction {
    type:string;
    payload: any
}

export interface currencyState {
    allCurrency: dropdownInterface[];
    searchCurrency: dropdownInterface[];
    tempAllCurrency: dropdownInterface[]
}

export interface dropdownInterface {
    country: string;
    flag: string;
    name: string;
    currency: string;
    isSelected: boolean;
}