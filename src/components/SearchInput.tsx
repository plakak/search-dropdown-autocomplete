import React, { ChangeEvent, useEffect } from "react";
import "../app/globals.css";
import {
  dropdownInterface,
} from "@/reducer/currencyReducer";
import { usePopper } from "react-popper";
import { selectActionInterface } from "@/app/page";

const SearchBox: React.FC<SearchProps> = ({
  label,
  description,
  value,
  callback,
  selectAction,
  isAsync = false,
  isLoading = false,
  disabledInput = false
}) => {
  const dropdownRef = React.useRef<HTMLDivElement>(null);

  const [inputElement, setInputElement] = React.useState<HTMLInputElement | null>(null);
  const [popperElement, setPopperElement] = React.useState<HTMLDivElement | null>(null);

  const { styles, attributes } = usePopper(inputElement, popperElement, {
    placement: "bottom-start",
    modifiers: [
      {
        name: "offset",
        options: {
          offset: [0, 0],
        },
      },
    ],
  });

  const [isShowDropdown, setIsShowDropdown] = React.useState<boolean>(false);
  const [highlightedIndex, setHighlightedIndex] = React.useState<number | null>(
    null
  );


  useEffect(() => {
    if (dropdownRef) {
      dropdownRef.current?.addEventListener("click", handleOutsideClick);
    }
    document.addEventListener("mousedown", handleOutsideClick);

    return () => {
      if (dropdownRef) {
        dropdownRef.current?.removeEventListener("click", handleOutsideClick);
      }
      document.addEventListener("mousedown", handleOutsideClick);
    };
  }, [dropdownRef]);

  React.useEffect(() => {
    if(value.length != 0 && isAsync){
      setIsShowDropdown(true);
    }
  },[value])

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (callback) {
      callback(event);
    }
    return event;
  };

  const handleInputFocus = () => {
    if (value.length != 0 && isAsync) {
      setIsShowDropdown(true);
    }

    if (!isAsync) {
      setIsShowDropdown(true);
    }
  };

  const handleOutsideClick = (event: MouseEvent) => {
    const target = event.target as Node;

    if (dropdownRef && !dropdownRef.current?.contains(target)) {
      setIsShowDropdown(false);
    }
  };

  const handleResultClick = (clickedItem: dropdownInterface) => {
    const updatedResults = value.map((item: dropdownInterface) =>
      item.country === clickedItem.country
        ? { ...item, isSelected: !item.isSelected }
        : item
    );
    if (selectAction) {
      if (isAsync) {
        selectAction({ type: "async", data: updatedResults });
      } else {
        selectAction({ type: "sync", data: updatedResults });
      }
    }
  };
  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "ArrowUp" || event.key === "ArrowDown") {
      event.preventDefault();
      const nextIndex =
        highlightedIndex === null
          ? event.key === "ArrowUp"
            ? value.length - 1
            : 0
          : event.key === "ArrowUp"
          ? (highlightedIndex - 1 + value.length) % value.length
          : (highlightedIndex + 1) % value.length;
      setHighlightedIndex(nextIndex);
    } else if (event.key === "Enter") {
      event.preventDefault();
      if (
        highlightedIndex !== null &&
        highlightedIndex >= 0 &&
        highlightedIndex < value.length
      ) {
        handleResultClick(value[highlightedIndex]);
      }
    } else if (event.key === "Escape") {
      inputElement?.blur();
      setIsShowDropdown(false);
    }
  };

  return (
    <div className="flex items-center min-w-80">
      <div className="grid w-full">
        <label htmlFor={label}>{label}</label>

        <div className="relative">
          <svg
            className="absolute left-3 top-3 h-5 w-5 text-gray-400"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M10 20a8 8 0 100-16 8 8 0 000 16z"
            />
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M21 21l-5-5"
            />
          </svg>
          <input
            id={label}
            type="text"
            onChange={handleInputChange}
            placeholder="Type to search"
            className="border rounded pl-10 pr-4 py-2 focus:outline-none focus:border-blue-500 w-full"
            autoComplete="off"
            onFocus={() => handleInputFocus()}
            ref={setInputElement}
            onKeyDown={handleKeyDown}
            disabled={disabledInput}
          />
          {isAsync && isLoading && (
            <div className="absolute inset-y-0 right-3 pl-2 flex items-center pointer-events-none">
              <svg
                className="animate-spin h-5 w-5 text-gray-500"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <circle
                  className="opacity-25"
                  cx="12"
                  cy="12"
                  r="10"
                  stroke="currentColor"
                  strokeWidth="4"
                ></circle>
                <path
                  className="opacity-75"
                  fill="currentColor"
                  d="M4 12a8 8 0 018-8V2.5M20 12a8 8 0 01-8 8V21.5"
                  stroke="currentColor"
                  strokeWidth="4"
                ></path>
              </svg>
            </div>
          )}
        </div>
        {isShowDropdown && (
          <div
            ref={setPopperElement}
            style={styles.popper}
            {...attributes.popper}
            className="z-10"
          >
            <div
              ref={dropdownRef}
              className="bg-white border border-gray-300 mt-1 w-full rounded max-h-64 overflow-auto"
            >{
              value.length ==0 && (
                <div className="w-80 p-2">
                  No result were found
                </div>
              )
            }
              {value.map((item, index) => (
                <div
                  key={index}
                  className={
                    index === highlightedIndex
                      ? "flex justify-between w-80 p-2 bg-blue-100"
                      : "flex justify-between w-80 p-2 even:bg-gray-100 odd:bg-white hover:bg-blue-100"
                  }
                  onClick={() => handleResultClick(item)}
                  onMouseEnter={() => setHighlightedIndex(index)}
                >
                  {isAsync ? (
                    <>
                    <div className="flex justify-between items-center w-full">
                      <div>
                      <span>{item.flag} {item.name}</span>
                      <br></br>
                      <span>Currency: {item.currency}</span>
                      </div>
                      <input
                        type="checkbox"
                        checked={item.isSelected}
                        onChange={() => handleResultClick(item)}
                        className="mr-2"
                      />
                    </div>
                    </>
                  ) : (
                    <>
                      <span>{item.name}</span>
                      <input
                        type="checkbox"
                        checked={item.isSelected}
                        onChange={() => handleResultClick(item)}
                        className="mr-2"
                      />
                    </>
                  )}
                </div>
              ))}
            </div>
          </div>
        )}
        <text>{description}</text>
      </div>
    </div>
  );
};

interface SearchProps {
  label: string;
  description: string;
  callback?: (event: ChangeEvent<HTMLInputElement>) => void;
  value: dropdownInterface[];
  selectAction?: (event: selectActionInterface) => void;
  isAsync: boolean;
  isLoading? :boolean;
  disabledInput?: boolean
}

export default SearchBox;
