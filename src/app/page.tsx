'use client'
import React, { useReducer } from "react";
import SearchInput from "@/components/SearchInput";
import axios from "axios";
import { ChangeEvent } from "react";
import { currencyReducer, dropdownInterface } from "@/reducer/currencyReducer";
import Toggle from "@/components/Toggle";

export default function Home() {

  const initialized = React.useRef<boolean>(false)

  const [searchTerm, setSearchTerm] = React.useState<string>('')
  const [loading, setLoading] = React.useState<boolean>(false)
  const [inputDisabled, setInputDisabled] = React.useState<InputDisabledInterface>({asycInput: false, syncInput: false})
  const [debouncedSearchTerm, setDebouncedSearchTerm] = React.useState<string>('')

  const [state, dispatch] = useReducer(currencyReducer, { allCurrency: [],searchCurrency: [], tempAllCurrency: [] });

  React.useEffect(() => {
    const initital = async() => {
      let response = await axios.get<currencyInterface[]>(`${process.env.NEXT_PUBLIC_REST_API}/all`)

      if(response.data){
        let preparedData = prepareData(response.data)
        dispatch({type:'initial', payload: preparedData})
      }
    }
    if(!initialized.current){
      initialized.current = true
      initital()
    }
     
  },[])

  React.useEffect(() => {
    const delaySearch = setTimeout(() => {
      setDebouncedSearchTerm(searchTerm);
    }, 500);

    return () => clearTimeout(delaySearch);
  },[searchTerm])

  React.useEffect(() => {
    const search = async() => {
      let response = await axios.get(`${process.env.NEXT_PUBLIC_REST_API}/name/${searchTerm}`)
      
      if(response.data){
        let preparedData = prepareData(response.data)
        dispatch({type: 'search', payload: preparedData})
        setLoading(false)
      }
    }

    if (debouncedSearchTerm) {
      setLoading(true)
      search()
    }
    else {
      dispatch({type: 'search', payload: []})
    }
  }, [debouncedSearchTerm]);

  const handleAsyncSearch = async (event: ChangeEvent<HTMLInputElement>) => {
    let name = event.target.value
      setSearchTerm(name)

  }

  const handleSyncSearch = async (event: ChangeEvent<HTMLInputElement>) => {
    let name = event.target.value
    let yy = state.allCurrency.filter((item: dropdownInterface) => item.country.toLowerCase().includes(name.toLowerCase()))

    dispatch({type: 'filter', payload: yy})
  }

  const handleSelect = (event: selectActionInterface) => {
    switch (event.type) {
      case 'async':
        dispatch({type: 'search', payload: event.data})
        break;
    
      default:
        dispatch({type: 'filter', payload: event.data})
        break;
    }
    
  }

  const handelToggleChange = (event: ChangeEvent<HTMLInputElement>, type: string) => {
  
    let checked = event.target.checked
    switch (type) {
      case 'async':
        setInputDisabled({...inputDisabled, asycInput: !checked})
        break;
    
      default:
        setInputDisabled({...inputDisabled, syncInput: !checked})
        break;
    }
  }

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24 bg-gray-200">
      <div className="bg-white rounded p-12">
        <div className="lg:col-span-2">
          <div className="grid gap-y-2 text-sm grid-cols-1 md:grid-cols-5">
            <div className="md:col-span-5">
              <Toggle
                label="Async Input Disabled"
                callback={(e) => handelToggleChange(e, "async")}
              />
              <SearchInput
                label="Async Search"
                description="With description and custom results display"
                callback={handleAsyncSearch}
                selectAction={handleSelect}
                value={state.searchCurrency}
                isAsync={true}
                isLoading={loading}
                disabledInput={inputDisabled.asycInput}
              />
            </div>
            <div className="md:col-span-5">
              <Toggle
                label="Sync Input Disabled"
                callback={(e) => handelToggleChange(e, "sync")}
              />

              <SearchInput
                label="Sync Search"
                description="With default display and search on focus"
                value={state.tempAllCurrency}
                callback={handleSyncSearch}
                selectAction={handleSelect}
                isAsync={false}
                disabledInput={inputDisabled.syncInput}
              />
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}


interface currencyInterface {
  name: Name;
  currencies?: any;
  flag: string;
}

interface Name {
  common: string
  official: string
}
interface Symbol {
  name: string;
  symbol: string;
}

interface InputDisabledInterface {
  asycInput: boolean;
  syncInput: boolean;
}

export interface selectActionInterface {
  type: "async" | 'sync';
  data: dropdownInterface[]
}

const capitalizeEachWord = (string: string) => {
  const stringArray = string.split(" ")
  let capitalizeString = stringArray.map((word:string) => {
    return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase()
  })
  
  return capitalizeString.join(" ")
}

const prepareData = (data: currencyInterface[]) => {
  let temp:dropdownInterface[] = []
  data.forEach((item:currencyInterface) => {
    let country = item.name.common
    if(item.currencies){
     
      let entries:Array<[string, Symbol]>  = Object.entries(item.currencies)
      if(entries.length > 1){
        entries.forEach((enItem) => {
          temp.push({
            country,
            flag: item.flag,
            currency: enItem[0],
            name: capitalizeEachWord(enItem[1].name),
            isSelected: false
          })
        })
      }
      else {
        temp.push({
          country,
          flag: item.flag,
          currency: entries[0][0],
          name: capitalizeEachWord(entries[0][1].name),
          isSelected: false
        })
      }
    }
  })

  

  return temp
}